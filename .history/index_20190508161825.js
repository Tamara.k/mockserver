var Express = require('express');
var bodyParser = require('body-parser');

var app = Express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var items = require('./data/shop-items');
var user = require('./data/user');

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.get("/shop-items", function(req, res) {
    res.json(items);
});
// http://localhost:3030/shop-items/1
app.get("/shop-items/:id", function(req, res) {
    // var foundItem = items.find(item => item.itemID === req.params.id);
    var foundItem = items.find(function (item) {
        console.log(item);
        return item.itemID === req.params.id;
    });
    console.log(foundItem);
    res.json(foundItem);
});

app.get("/user", function(req, res) {
    res.json(user);
});

app.listen(3030, function(a) {
    console.log("Listening to port 3030");
});

